import Vue from 'vue';
import VueRouter, {RouterOptions} from 'vue-router';
import Home from '../views/Home.vue';
import MatchupClient from '../views/MatchupClient.vue';
import StreamOverlay from '../views/StreamOverlay.vue';
import WideOverlay from '../views/WideOverlay.vue';
import TournamentOverlay from '../views/TournamentOverlay.vue';
import VerticalOverlay from '../views/VerticalOverlay.vue';
import ExtraLifeOverlay from '../views/ExtraLifeOverlay.vue';
import DiscordAuth from '../views/DiscordAuth.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    props: true
  },
  {
    path: '/auth',
    name: 'auth',
    component: DiscordAuth
  },
  {
    path: '/matchup/:matchupId/:playerName',
    name: 'matchup-client',
    component: MatchupClient,
    props: true
  },
  {
    path: '/classic-overlay/:matchupId/:overlayOptions?',
    name: 'classic-overlay',
    component: StreamOverlay,
    props: true
  },
  {
    path: '/overlay/:matchupId',
    name: 'stream-overlay',
    component: WideOverlay,
    props: true
  },
  {
    path: '/vertical-overlay/:matchupId',
    name: 'vertical-overlay',
    component: VerticalOverlay,
    props: true
  },
  {
    path: '/tournament-overlay/:matchupId',
    name: 'tournament-overlay',
    component: TournamentOverlay,
    props: true
  },
  {
    path: '/el-overlay/:matchupId',
    name: 'el-overlay',
    component: ExtraLifeOverlay,
    props: true
  }
];

const router = new VueRouter(<RouterOptions> {
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
