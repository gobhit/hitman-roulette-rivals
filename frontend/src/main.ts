import Vue from 'vue';
import App from './App.vue';
import router from './router';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios';
import VueMoment from 'vue-moment';
import moment from 'moment-timezone';
import VueWamp from 'vue-wamp';
import VueCookies from 'vue-cookies';

Vue.config.productionTip = false;
Vue.prototype.$http = axios;
Vue.prototype.$domain = window.location.hostname.includes('localhost') ?
    'http://localhost:8000' :
    `${document.location.protocol}//${window.location.hostname}`;
const port = document.location.port ? `:${document.location.port}`: '';
Vue.prototype.$vueDomain = `${document.location.protocol}//${window.location.hostname}${port}`;

Vue.use(VueMoment, {
  moment
});

Vue.use(VueWamp, {
  url: 'wss://ws.hitmaps.com',
  realm: 'realm1',
  auto_reestablish: true,
  auto_close_timeout: -1,
});
Vue.use(VueCookies);

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app');
