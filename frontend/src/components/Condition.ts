export default class Condition {
    public name: string = '';
    public tileUrl: string = '';
    public chosen: boolean = false;

    constructor(init?: Partial<Condition>) {
        Object.assign(this, init);
    }
}
