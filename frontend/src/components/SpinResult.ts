import Condition from '@/components/Condition';

export default class SpinResult {
    public target: any;
    public killMethod: Condition = new Condition();
    public disguise: Condition = new Condition();

    constructor(init?: Partial<SpinResult>) {
        Object.assign(this, init);
    }
}
