<template>
    <div
        class="modal game-modal fade"
        tabindex="-1"
        role="dialog"
        aria-hidden="true"
        v-bind="$attrs"
        :id="id"
    >
        <div class="modal-dialog modal-xl" role="document" :class="fullscreen ? 'fullscreen' : ''">
            <div class="modal-content">
                <div class="modal-header">
                    <h5
                        class="modal-title"
                        :id="id"
                        v-if="modalTitle"
                        v-html="modalTitle"
                    ></h5>
                    <button
                        type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-label="Close"
                        v-if="dismissable"
                    >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body d-flex flex-column">
                    <slot></slot>
                </div>
                <div class="modal-footer">
                    <slot name="modal-footer"></slot>
                </div>
            </div>
        </div>
    </div>
</template>

<script>
export default {
    name: 'Modal',
    props: {
        id: String,
        dismissable: Boolean,
        fullscreen: Boolean,
        modalTitle: String
    }
}
</script>

<style lang="scss">
.game-modal {
    .modal-dialog {
        &.fullscreen {
            width: 95%;
            max-width: 95%;
        }

        .modal-content {
            background: transparent;
            border: none;

            .modal-header {
                font-family: 'nimbus_sans_lbold', sans-serif;
                text-transform: uppercase;
                border-bottom: none;
                border-radius: 0;

                background: #2d2f39;
                color: white;

                h5 {
                    font-size: 1.5rem;
                }

                .close {
                    color: white;
                }
            }

            .modal-body {
                background: #2d2f39;
                color: white;

                td {
                    color: white;
                }
            }

            .modal-footer {
                border-top: none;
                padding-left: 0;
                padding-right: 0;
                padding-top: 10px;
                display: block;

                > :not(:first-child) {
                    margin: 0;
                }

                #enroll-button {
                    margin-bottom: 10px;
                }
            }
        }
    }
}
</style>
