export default class Mission {
    public game!: string;
    public name!: string;
    public gameUri!: string;
    public location!: string;
    public locationName!: string;
    public mainDifficulty!: string;
    public tileUrl!: string;
    public locationTileUrl!: string;

    constructor(init?: Partial<Mission>) {
        Object.assign(this, init);
    }
}
