export default class Utils {
    public static websocketsSupported() {
        return 'WebSocket' in window || 'MozWebSocket' in window;
    }
}
