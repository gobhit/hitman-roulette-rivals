<?php

use DataAccess\Models\SpinHistory;
use DI\Container;
use Doctrine\ORM\EntityManager;
use HitmapsRoulette\BusinessLogic\CriteriaFilters;
use HitmapsRoulette\BusinessLogic\GeneratedSpinToInGameContractTransformer;
use HitmapsRoulette\BusinessLogic\RouletteSpinner;
use HitmapsRoulette\DataAccess\Models\RouletteMatchup;
use Klein\Klein;
use Klein\Request;
use Klein\Response;
use Predis\Client;

require 'src/bootstrap.php';

$klein = new Klein();

$klein->respond(function(Request $request, Response $response) use ($applicationContext) {
    if (isset($_SERVER['HTTP_ORIGIN'])) {
        $response->header('Access-Control-Allow-Origin', $_SERVER['HTTP_ORIGIN']);
    }
    $response->header('Access-Control-Allow-Headers', 'content-type,Authorization');
    $response->header('Access-Control-Allow-Credentials', 'true');
    $response->header('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE, OPTIONS');
});

$klein->respond('POST', '/api/spins', function(Request $request, Response $response) use ($applicationContext) {
    $requestBody = json_decode($request->body(), true);
    $forGameClient = isset($requestBody['forGameClient']) && $requestBody['forGameClient'];

    $spinDetails = $applicationContext->get(RouletteSpinner::class)->spin(
        $requestBody['missionPool'],
        CriteriaFilters::fromJson($requestBody['criteriaFilters']),
        $forGameClient);

    if ($forGameClient) {
        return $response->json($applicationContext->get(GeneratedSpinToInGameContractTransformer::class)
            ->transform($spinDetails));
    }

    return $response->json($spinDetails);
});

$klein->respond('GET', '/api/matchups/[:matchupId]', function(Request $request, Response $response) use ($applicationContext) {
    $matchupId = $request->matchupId;

    $playerName = isset($_GET['name']) ? $_GET['name'] : '';
    $matchup = getMatchupInformation($matchupId, $applicationContext, $playerName);

    if ($matchup !== null) {
        return $response->json($matchup);
    }

    return $response->code(404);
});

function getMatchupInformation($matchupId, Container $applicationContext, $playerName = '') {
    /* @var $matchup RouletteMatchup */
    $entityManager = $applicationContext->get(EntityManager::class);
    $matchup = $entityManager
        ->getRepository(RouletteMatchup::class)
        ->findOneBy(['matchupId' => $matchupId]);

    if ($playerName !== '') {
        $currentMillisecond = microtime(true) * 1000;
        if ($matchup->getPlayerOneName() === $playerName) {
            $matchup->setPlayerOneLastPing($currentMillisecond);
        } elseif ($matchup->getPlayerTwoName() === $playerName) {
            $matchup->setPlayerTwoLastPing($currentMillisecond);
        }
        $entityManager->persist($matchup);
        $entityManager->flush();
    }

    $matchup->currentTime = new DateTime('now', new DateTimeZone('UTC'));
    $matchup->remainingTimeInSeconds = calculateRemainingMatchTime($matchup);
    $matchup->pretime = $matchup->currentTime < $matchup->getSpinTime();
    $matchup->remainingPretimeInSeconds = calculatePretimeRemaining($matchup);
    $matchup->showTimer = $matchup->getMatchLength() !== 'NO TIME LIMIT';

    // Formatting
    $matchup->formattedCurrentTime = $matchup->currentTime->format(DATE_ISO8601);
    $matchup->formattedSpinTime = $matchup->getSpinTime()->format(DATE_ISO8601);
    $matchup->formattedPlayerOneCompleteTime = $matchup->getPlayerOneCompleteTime() ? $matchup->getPlayerOneCompleteTime()->format(DATE_ISO8601) : null;
    $matchup->formattedPlayerTwoCompleteTime = $matchup->getPlayerTwoCompleteTime() ? $matchup->getPlayerTwoCompleteTime()->format(DATE_ISO8601) : null;

    return $matchup;
}

function calculateRemainingMatchTime(RouletteMatchup $matchup): int {
    if ($matchup->getMatchLength() === 'NO TIME LIMIT') {
        return -1;
    }

    $matchEndTime = (clone $matchup->getSpinTime())->modify($matchup->getMatchLength());
    $difference = $matchEndTime->diff($matchup->currentTime);

    if ($difference->invert === 0) {
        return 0;
    }

    return ($difference->h * 60 * 60) + ($difference->i * 60) + $difference->s;
}

function calculatePretimeRemaining(RouletteMatchup $matchup) {
    $difference = $matchup->getSpinTime()->diff($matchup->currentTime);

    if ($difference->invert === 0) {
        return 0;
    }

    return ($difference->h * 60 * 60) + ($difference->i * 60) + $difference->s;
}

$klein->respond('POST', '/api/matchups', function(Request $request, Response $response) use ($applicationContext) {
    $requestBody = json_decode($request->body(), true);

    if ($requestBody === null) {
        return $response->json(['message' => 'Could not decode JSON'])->code(400);
    }

    $matchupId = '';
    $attempt = 0;
    while ($attempt < 20) {
        $matchupId = preg_replace("/[^A-Za-z0-9 ]/", '', uniqid('', true));

        $existingMatchup = $applicationContext->get(EntityManager::class)
            ->getRepository(RouletteMatchup::class)
            ->findOneBy(['matchupId' => $matchupId]);

        if ($existingMatchup === null) {
            break;
        }

        $attempt++;
    }

    $rouletteMatchup = new RouletteMatchup();
    $rouletteMatchup->setMatchupData('');
    $rouletteMatchup->setMatchupId($matchupId);
    $rouletteMatchup->setPlayerOneName($requestBody['playerOneName']);
    $rouletteMatchup->setPlayerTwoName($requestBody['playerTwoName']);
    $rouletteMatchup->setPlayerOneLastPing(0);
    $rouletteMatchup->setPlayerTwoLastPing(0);
    $rouletteMatchup->setTournamentMatchId($requestBody['tournamentMatchId']);
    $spinTime = new DateTime('now', new DateTimeZone('UTC'));
    $spinTime->modify('-1 day');
    $rouletteMatchup->setSpinTime($spinTime);
    $rouletteMatchup->setMatchLength('60 minutes');

    $applicationContext->get(EntityManager::class)->persist($rouletteMatchup);
    $applicationContext->get(EntityManager::class)->flush();

    return $response->json(['matchupId' => $matchupId]);
});

$klein->respond('PATCH', '/api/matchups/[:matchupId]', function(Request $request, Response $response) use ($applicationContext) {
    $requestBody = json_decode($request->body(), true);

    if ($requestBody === null) {
        $response->code(400);
        return $response->json(['message' => 'Could not decode JSON']);
    }

    /* @var $matchup RouletteMatchup|null */
    $matchup = $applicationContext->get(EntityManager::class)->getRepository(RouletteMatchup::class)->findOneBy(['matchupId' => $request->matchupId]);
    if ($matchup === null) {
        return $response->code(404);
    }

    /** @noinspection PhpUnusedLocalVariableInspection */
    $responseProperty = '';
    $decodedMatchupData = json_decode($requestBody['matchupData'], true);
    if (isset($requestBody['matchupData']) && $decodedMatchupData !== null) {
        $matchup->setMatchupData($requestBody['matchupData']);

        if ($requestBody['clearWinner']) {
            $matchup->setWinner(null);
        }

        $matchup->setPlayerOneCompleteTime(null);
        $matchup->setPlayerTwoCompleteTime(null);
        if ($decodedMatchupData['show']) {
            $spinTime = new DateTime('now', new DateTimeZone('UTC'));
            if ($decodedMatchupData['matchTime'] && $spinTime < new DateTime($decodedMatchupData['matchTime'])) {
                $spinTime = new DateTime($decodedMatchupData['matchTime']);
            }
            $matchup->setSpinTime($spinTime);

            if (intval($decodedMatchupData['matchDuration']) !== -1) {
                $matchup->setMatchLength("{$decodedMatchupData['matchDuration']} minutes");
            } else {
                $matchup->setMatchLength('NO TIME LIMIT');
            }
        } else {
            $now = new DateTime('now', new DateTimeZone('UTC'));
            $now->modify('-2 hours');
            $matchup->setSpinTime($now);
        }

        $responseProperty = 'matchupData';
    } elseif (isset($requestBody['lastPing']) && isset($requestBody['playerName'])) {
        if ($requestBody['playerName'] === $matchup->getPlayerOneName()) {
            $matchup->setPlayerOneLastPing($requestBody['lastPing']);
        } elseif ($requestBody['playerName'] === $matchup->getPlayerTwoName()) {
            $matchup->setPlayerTwoLastPing($requestBody['lastPing']);
        } else {
            $response->code(400);
            return $response->json(['message' => 'Could not find player!']);
        }
        $responseProperty = 'lastPing';
    } elseif (isset($requestBody['action'])) {
        $responseProperty = 'action';

        switch ($requestBody['action']) {
            case 'complete':
                $now = new DateTimeImmutable('now', new DateTimeZone('UTC'));
                if ($requestBody['player'] === $matchup->getPlayerOneName()) {
                    $matchup->setPlayerOneCompleteTime($now);
                } elseif ($requestBody['player'] === $matchup->getPlayerTwoName()) {
                    $matchup->setPlayerTwoCompleteTime($now);
                } else {
                    $response->code(400);
                    return $response->json(['message' => 'Malformed request']);
                }
                break;
            case 'verify':
                if ($requestBody['player'] === $matchup->getPlayerOneName()) {
                    $winner = 1;
                } elseif ($requestBody['player'] === $matchup->getPlayerTwoName()) {
                    $winner = 2;
                } else {
                    $response->code(400);
                    return $response->json(['message' => 'Malformed request']);
                }
                $matchup->setWinner($winner);
                $matchup->setPlayerOneCompleteTime(null);
                $matchup->setPlayerTwoCompleteTime(null);
                break;
            case 'reject':
                if ($requestBody['player'] === $matchup->getPlayerOneName()) {
                    $matchup->setPlayerOneCompleteTime(null);
                } elseif ($requestBody['player'] === $matchup->getPlayerTwoName()) {
                    $matchup->setPlayerTwoCompleteTime(null);
                } else {
                    $response->code(400);
                    return $response->json(['message' => 'Malformed request']);
                }
                break;
        }
    } else {
        $response->code(400);
        return $response->json(['message' => 'Request JSON not supported.']);
    }

    $applicationContext->get(EntityManager::class)->persist($matchup);
    $applicationContext->get(EntityManager::class)->flush();

    switch ($responseProperty) {
        case 'matchupData':
            $pushBody = getMatchupInformation($matchup->getMatchupId(), $applicationContext);
            $matchupData = json_decode($pushBody->matchupData, true);
            break;
        default:
            $pushBody = null;
            break;
    }

    if ($pushBody !== null) {
        try {
            $applicationContext->get(Client::class)->publish('ws', json_encode([
                'type' => 'matchupData',
                'key' => $matchup->getMatchupId(),
                'data' => $pushBody
            ]));
        } catch (Exception $e) {
            //-- Nothing for now. Just don't want to break anything :P
        }
    }

    return $response->json($requestBody[$responseProperty]);
});

$klein->onHttpError(function (int $code, Klein $router) {
    $router->response()->code($code);
    switch ($code) {
        case 403:
            $router->response()->json([
                'message' => 'Forbidden',
                'uri' => $router->request()->uri()
            ]);
            break;
        case 404:
            $router->response()->json([
                'message' => "Could not find route with URI {$router->request()->uri()}",
                'uri' => $router->request()->uri()
            ]);
            break;
        case 500:
            $router->response()->json([
                'message' => 'It appears that something went horribly wrong, and we are unable to handle your request at this time. Please try again in a few moments.',
                'uri' => $router->request()->uri()
            ]);
            break;
        default:
            $router->response()->json([
                'message' => "Welp, something unexpected happened with error code: {$code}",
                'uri' => $router->request()->uri()
            ]);
    }
});

$klein->onError(function (Klein $klein, $msg, $type, Throwable $err) {
    error_log($err);
    //\Rollbar\Rollbar::log(\Rollbar\Payload\Level::ERROR, $err);
    $klein->response()->code(500);

    $klein->response()->json([
        'message' => 'It appears that something went horribly wrong, and we are unable to handle your request at this time. Please try again in a few moments.',
        'uri' => $klein->request()->uri()
    ]);
});

$klein->dispatch();
