<?php


namespace HitmapsRoulette\DataAccess\Models;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Doctrine\ORM\PersistentCollection;

/**
 * @ORM\Entity()
 * @ORM\Table(name="disguise_to_ioi_guid")
 */
class DisguiseToIoiGuid {
    /**
     * @ORM\Id() @ORM\Column(type="integer") @ORM\GeneratedValue()
     */
    public $id;

    /**
     * @ORM\Column(type="integer", name="mission_id")
     */
    public $missionId;

    /**
     * @ORM\Column(type="string", name="disguise_name")
     */
    public $disguiseName;

    /**
     * @ORM\Column(type="string", name="ioi_guid")
     */
    public $ioiGuid;

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getMissionId() {
        return $this->missionId;
    }

    /**
     * @param mixed $missionId
     */
    public function setMissionId($missionId): void {
        $this->missionId = $missionId;
    }

    /**
     * @return mixed
     */
    public function getDisguiseName() {
        return $this->disguiseName;
    }

    /**
     * @param mixed $disguiseName
     */
    public function setDisguiseName($disguiseName): void {
        $this->disguiseName = $disguiseName;
    }

    /**
     * @return mixed
     */
    public function getIoiGuid() {
        return $this->ioiGuid;
    }

    /**
     * @param mixed $ioiGuid
     */
    public function setIoiGuid($ioiGuid): void {
        $this->ioiGuid = $ioiGuid;
    }
}
