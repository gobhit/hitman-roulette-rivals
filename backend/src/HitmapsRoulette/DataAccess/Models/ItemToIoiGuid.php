<?php


namespace HitmapsRoulette\DataAccess\Models;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Doctrine\ORM\PersistentCollection;

/**
 * @ORM\Entity()
 * @ORM\Table(name="item_to_ioi_guid")
 */
class ItemToIoiGuid {
    /**
     * @ORM\Id() @ORM\Column(type="integer") @ORM\GeneratedValue()
     */
    public $id;

    /**
     * @ORM\Column(type="string", name="item_name")
     */
    public $itemName;

    /**
     * @ORM\Column(type="string", name="ioi_guid")
     */
    public $ioiGuid;

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getItemName() {
        return $this->itemName;
    }

    /**
     * @param mixed $itemName
     */
    public function setItemName($itemName): void {
        $this->itemName = $itemName;
    }

    /**
     * @return mixed
     */
    public function getIoiGuid() {
        return $this->ioiGuid;
    }

    /**
     * @param mixed $ioiGuid
     */
    public function setIoiGuid($ioiGuid): void {
        $this->ioiGuid = $ioiGuid;
    }
}
