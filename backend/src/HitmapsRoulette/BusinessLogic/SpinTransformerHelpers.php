<?php


namespace HitmapsRoulette\BusinessLogic;


use Doctrine\ORM\EntityManager;
use HitmapsRoulette\BusinessLogic\CommonKills\CommonKillVariant;
use HitmapsRoulette\DataAccess\Models\ItemToIoiGuid;

class SpinTransformerHelpers {
    private $entityManager;

    public function __construct(EntityManager $entityManager) {
        $this->entityManager = $entityManager;
    }

    public function isKillMethodBroad(?string $ioiName): bool {
        return in_array($ioiName, ['pistol', 'sniperrifle', 'assaultrifle', 'smg', 'shotgun', 'explosive', 'unarmed', 'fiberwire']);
    }

    public function transformSpecificMelee(string $targetGuid, SpinResult $spinResult, string $objectiveGuid, string $jsonType): string {
        /* @var $weaponMapping ItemToIoiGuid */
        $weaponMapping = $this->entityManager->getRepository(ItemToIoiGuid::class)
            ->findOneBy(['itemName' => $spinResult->killMethod->name]);
        if ($weaponMapping === null) {
            throw new Exception("Could not find weapon for name '{$spinResult->killMethod->name}'!");
        }

        if ($jsonType === 'TargetCondition') {
            $targetConditionJson = file_get_contents(__DIR__ . '/Missions/MissionJson/Partial/TargetConditions/kill-specific-melee.json');
            $targetConditionJson = str_replace('{{PARAM-WEAPON-GUID}}', $weaponMapping->getIoiGuid(), $targetConditionJson);
            $targetConditionJson = str_replace('{{KILL-METHOD-GUID}}', $objectiveGuid, $targetConditionJson);

            return $targetConditionJson;
        }

        if ($jsonType === 'StateMachine') {
            $innerConditionJson = file_get_contents(__DIR__ . '/Missions/MissionJson/Partial/KillMethods/kill-method-specific-melee.json');
            $innerConditionJson = str_replace('{{PARAM-WEAPON-GUID}}', $weaponMapping->getIoiGuid(), $innerConditionJson);

            return $this->transformMainKillMethodStateMachine($spinResult, $targetGuid, $objectiveGuid, $innerConditionJson);
        }

        throw new Exception("Could not handle JSON type '{$jsonType}'");
    }

    public function transformKillMethodBroad(string $targetGuid, SpinResult $spinResult, string $objectiveGuid, string $jsonType): string {
        if ($jsonType === 'TargetCondition') {
            $targetConditionJson = file_get_contents(__DIR__ . '/Missions/MissionJson/Partial/TargetConditions/kill-firearm-or-accident.json');
            $targetConditionJson = str_replace('{{KILL-METHOD}}', $spinResult->killMethod->ioiMethod, $targetConditionJson);
            $targetConditionJson = str_replace('{{KILL-METHOD-GUID}}', $objectiveGuid, $targetConditionJson);

            return $targetConditionJson;
        }

        if ($jsonType === 'StateMachine') {
            $file = 'killmethodbroad';
            if ($spinResult->killMethod->ioiMethod === 'pistol') {
                $file = 'pistol';
            } elseif ($spinResult->killMethod->ioiMethod === 'smg') {
                $file = 'smg';
            } elseif ($spinResult->killMethod->ioiMethod === 'sniperrifle' && $spinResult->target->name === 'Sierra Knox') {
                $file = 'sierra-knox-sniperrifle';
            }

            $innerConditionJson = file_get_contents(__DIR__ . "/Missions/MissionJson/Partial/KillMethods/kill-method-{$file}.json");
            $innerConditionJson = str_replace('{{BROAD-KILL-METHOD}}', $spinResult->killMethod->ioiMethod, $innerConditionJson);

            $selectedVariant = $this->getChosenVariant($spinResult->killMethod->variants);
            $variantJson = file_get_contents(__DIR__ . "/Missions/MissionJson/Partial/KillMethods/kill-method-variant.json");
            if ($selectedVariant !== null) {
                if ($spinResult->killMethod->ioiMethod === 'explosive') {
                    $variantJson = file_get_contents(__DIR__ . "/Missions/MissionJson/Partial/KillMethods/kill-method-variant-loud-explosive-weapon.json");
                } else {
                    $variantJson = str_replace('{{KILL-METHOD-VARIANT-KEY}}', '$Value.WeaponSilenced', $variantJson);
                    $variantJson = str_replace('"{{KILL-METHOD-VARIANT-VALUE}}"',
                        $selectedVariant->name === 'Loud' ? 'false' : 'true',
                        $variantJson);
                }
                $innerConditionJson = str_replace('"{{KILL-METHOD-VARIANT}}"', $variantJson, $innerConditionJson);
            } else {
                $innerConditionJson = str_replace('"{{KILL-METHOD-VARIANT}}",', '', $innerConditionJson);
            }

            return $this->transformMainKillMethodStateMachine($spinResult, $targetGuid, $objectiveGuid, $innerConditionJson);
        }

        throw new Exception("Could not handle JSON type '{$jsonType}'");
    }

    public function transformKillMethodStrict(string $targetGuid, SpinResult $spinResult, string $objectiveGuid, string $jsonType): string {
        if ($jsonType === 'TargetCondition') {
            $targetConditionJson = file_get_contents(__DIR__ . '/Missions/MissionJson/Partial/TargetConditions/kill-firearm-or-accident.json');
            $targetConditionJson = str_replace('{{KILL-METHOD}}', $spinResult->killMethod->ioiMethod, $targetConditionJson);
            $targetConditionJson = str_replace('{{KILL-METHOD-GUID}}', $objectiveGuid, $targetConditionJson);

            return $targetConditionJson;
        }

        if ($jsonType === 'StateMachine') {
            if ($spinResult->target->name === 'Viktor Novikov' && $spinResult->killMethod->ioiMethod === 'consumed_poison') {
                $innerConditionJson = file_get_contents(__DIR__ . '/Missions/MissionJson/Partial/KillMethods/kill-method-novikov-consumed-poison.json');
            } else {
                $innerConditionJson = file_get_contents(__DIR__ . '/Missions/MissionJson/Partial/KillMethods/kill-method-killmethodstrict.json');
            }
            $innerConditionJson = str_replace('{{STRICT-KILL-METHOD}}', $spinResult->killMethod->ioiMethod, $innerConditionJson);

            $selectedVariant = $this->getChosenVariant($spinResult->killMethod->variants);
            $variantJson = file_get_contents(__DIR__ . "/Missions/MissionJson/Partial/KillMethods/kill-method-variant.json");
            if ($selectedVariant !== null) {
                $variantJson = str_replace('{{KILL-METHOD-VARIANT-KEY}}', '$Value.WeaponSilenced', $variantJson);
                $variantJson = str_replace('"{{KILL-METHOD-VARIANT-VALUE}}"',
                    $selectedVariant->name === 'Loud' ? 'false' : 'true',
                    $variantJson);
                $innerConditionJson = str_replace('"{{KILL-METHOD-VARIANT}}"', $variantJson, $innerConditionJson);
            } else {
                $innerConditionJson = str_replace('"{{KILL-METHOD-VARIANT}}",', '', $innerConditionJson);
            }

            return $this->transformMainKillMethodStateMachine($spinResult, $targetGuid, $objectiveGuid, $innerConditionJson);
        }

        throw new Exception("Could not handle JSON type '{$jsonType}'");
    }

    /* @return array|CommonKillVariant
     * @var $variants CommonKillVariant[]
     */
    public function getChosenVariant(array $variants) {
        $selectedVariant = array_filter($variants, fn($el) => $el->chosen);

        return count($selectedVariant) ? $selectedVariant[0] : null;
    }

    public function transformMainKillMethodStateMachine(SpinResult $spinResult, string $targetGuid, string $objectiveGuid, string $innerConditionJson) {
        $variant = $this->getChosenVariant($spinResult->killMethod->variants);
        $jsonName = 'kill-method';
        if ($spinResult->target->name === 'Sierra Knox') {
            $jsonName = 'kill-method-sierra-knox';
        }
        $killMethodJson = file_get_contents(__DIR__ . "/Missions/MissionJson/Partial/{$jsonName}.json");
        $killMethodJson = str_replace('{{PARAM-TARGET-NAME}}', $spinResult->target->name, $killMethodJson);

        $killMethodName = $variant === null ?
            $spinResult->killMethod->name :
            "{$variant->name} {$spinResult->killMethod->name}";
        $killMethodJson = str_replace('{{PARAM-KILL-METHOD-NAME}}', $killMethodName, $killMethodJson);
        $killMethodJson = str_replace('{{KILL-METHOD-GUID}}', $objectiveGuid, $killMethodJson);
        $killMethodJson = str_replace('{{PARAM-TARGET-GUID}}', $targetGuid, $killMethodJson);

        return str_replace('"{{KILL-METHOD-JSON}}"', $innerConditionJson, $killMethodJson);
    }
}
