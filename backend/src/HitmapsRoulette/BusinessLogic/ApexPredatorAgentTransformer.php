<?php


namespace HitmapsRoulette\BusinessLogic;


use Doctrine\ORM\EntityManager;
use HitmapsRoulette\DataAccess\Models\ItemToIoiGuid;

class ApexPredatorAgentTransformer {
    private $spinTransformerHelpers;
    private $entityManager;

    public function __construct(SpinTransformerHelpers $spinTransformerHelpers,
                                EntityManager $entityManager) {
        $this->spinTransformerHelpers = $spinTransformerHelpers;
        $this->entityManager = $entityManager;
    }

    public function transform(string $guid, SpinResult $result, ?string $disguiseGuid): ApexPredatorTransformedAgent {
        $transformed = new ApexPredatorTransformedAgent();
        $variant = $this->spinTransformerHelpers->getChosenVariant($result->killMethod->variants);
        $killMethodName = $variant === null ?
            $result->killMethod->name :
            "{$variant->name} {$result->killMethod->name}";
        $json = file_get_contents(__DIR__ . '/Missions/MissionJson/Partial/target-ica-agent.json');
        $json = str_replace('{{TARGET-OBJECTIVE-GUID}}', $guid, $json);
        $json = str_replace('{{PARAM-KILL-METHOD-NAME}}', $killMethodName, $json);

        if ($result->disguise->name !== 'Any Disguise') {
            $json = str_replace('{{PARAM-DISGUISE-NAME}}', str_replace('"', '\\"', $result->disguise->name), $json);
        } else {
            $json = str_replace('\t\t\t\t\tWear Disguise: <b>{{PARAM-DISGUISE-NAME}}</b>', '', $json);
        }

        $variant = $this->spinTransformerHelpers->getChosenVariant($result->killMethod->variants);
        if ($variant === null) {
            $json = str_replace('"ObjectiveType": "customkill",', '', $json);
        }

        //region Disguise
        $disguiseMethodJson = '';
        if ($result->disguise->name !== 'Any Disguise') {
            $suit = $result->disguise->name === 'Suit';
            //region statemachine
            $disguiseMethodJson = file_get_contents(__DIR__ . '/Missions/MissionJson/Partial/Disguise/disguise-specific.json');
            $transformed->disguisePropertyNameReplaceQuotedParameter = false;
            $transformed->disguisePropertyNameReplaceValue = $suit ? '$Value.OutfitIsHitmanSuit' : '$Value.OutfitRepositoryId';
            $disguiseMethodJson = str_replace('{{DISGUISE-PROPERTY-NAME}}',
                $transformed->disguisePropertyNameReplaceValue,
                $disguiseMethodJson);

            $transformed->disguisePropertyValueReplaceQuotedParameter = $suit;
            $additionalDisguiseGuids = [];
            if (!$suit) {
                $additionalDisguiseGuids = $this->getAdditionalDisguiseGuidsForDisguise($result->disguise);
            }
            $formattedDisguiseGuids = $disguiseGuid;
            foreach ($additionalDisguiseGuids as $additionalDisguiseGuid) {
                $formattedDisguiseGuids .= '","' . $additionalDisguiseGuid;
            }

            $disguiseMethodJson = str_replace($suit ? '"{{DISGUISE-PROPERTY-VALUE}}"' : '{{DISGUISE-PROPERTY-VALUE}}',
                $suit ? 'true' : $formattedDisguiseGuids,
                $disguiseMethodJson);
            //endregion
        } else {
            $json = str_replace('"{{DISGUISE-METHOD-JSON}}",', '', $json);
            $disguiseMethodJson = 'REMOVE';
        }
        //endregion
        //region KillMethod
        if (str_contains($result->killMethod->ioiMethod, 'accident_') ||
            in_array($result->killMethod->ioiMethod, ['injected_poison', 'consumed_poison'])) {
            $weaponStateMachineJson = $this->transformKillMethodStrict($result);
        } elseif ($result->killMethod->ioiMethod === 'killmethod_specificitem') {
            $weaponStateMachineJson = $this->transformSpecificMelee($result);
        } elseif ($this->spinTransformerHelpers->isKillMethodBroad($result->killMethod->ioiMethod)) {
            $weaponStateMachineJson = $this->transformKillMethodBroad($result);
        } else {
            throw new Exception("Unable to handle objective kill method: '{$result->killMethod->ioiMethod}'");
        }
        //endregion

        //region Put it all together
        if ($result->disguise->name !== 'Any Disguise') {
            $escapedDisguise = str_replace('"', '\\"', $result->disguise->name);
            $disguiseNameSuffix = $result->disguise->name === 'Suit' ? '<b>a Suit</b>' : "disguise: <b>{$escapedDisguise}</b>";
            $json = str_replace('<b>{{PARAM-DISGUISE-NAME-SUFFIX}}</b>', $disguiseNameSuffix, $json);
            $json = str_replace('"{{DISGUISE-METHOD-JSON}}"', $disguiseMethodJson, $json);
        } else {
            $json = str_replace(' / {{PARAM-DISGUISE-NAME}}', '', $json);
            $json = str_replace(' while wearing <b>{{PARAM-DISGUISE-NAME-SUFFIX}}</b>', '', $json);
        }

        $json = str_replace('"{{KILL-METHOD-JSON}}"', $weaponStateMachineJson, $json);
        $json = str_replace('{{PARAM-TARGET-NAME}}', $result->target->name, $json);
        $json = str_replace('{{PARAM-KILL-METHOD-NAME}}', $killMethodName, $json);
        //endregion

        $transformed->targetJson = $json;
        $transformed->killCondition = $weaponStateMachineJson;
        $transformed->disguiseRestriction = $disguiseMethodJson;

        return $transformed;
    }

    private function transformKillMethodStrict(SpinResult $result): string {
        $jsonName = $result->killMethod->ioiMethod === 'consumed_poison' ? 'ica-agent-consumed-poison' : 'killmethodstrict';
        $innerConditionJson = file_get_contents(__DIR__ . "/Missions/MissionJson/Partial/KillMethods/kill-method-{$jsonName}.json");
        $innerConditionJson = str_replace('{{STRICT-KILL-METHOD}}', $result->killMethod->ioiMethod, $innerConditionJson);

        $selectedVariant = $this->spinTransformerHelpers->getChosenVariant($result->killMethod->variants);
        $variantJson = file_get_contents(__DIR__ . "/Missions/MissionJson/Partial/KillMethods/kill-method-variant.json");
        if ($selectedVariant !== null) {
            $variantJson = str_replace('{{KILL-METHOD-VARIANT-KEY}}', '$Value.WeaponSilenced', $variantJson);
            $variantJson = str_replace('"{{KILL-METHOD-VARIANT-VALUE}}"',
                $selectedVariant->name === 'Loud' ? 'false' : 'true',
                $variantJson);
            $innerConditionJson = str_replace('"{{KILL-METHOD-VARIANT}}"', $variantJson, $innerConditionJson);
        } else {
            $innerConditionJson = str_replace('"{{KILL-METHOD-VARIANT}}",', '', $innerConditionJson);
        }

        return $innerConditionJson;
    }

    private function transformSpecificMelee(SpinResult $result): string {
        /* @var $weaponMapping ItemToIoiGuid */
        $weaponMapping = $this->entityManager->getRepository(ItemToIoiGuid::class)
            ->findOneBy(['itemName' => $result->killMethod->name]);
        if ($weaponMapping === null) {
            throw new Exception("Could not find weapon for name '{$result->killMethod->name}'!");
        }

        $innerConditionJson = file_get_contents(__DIR__ . '/Missions/MissionJson/Partial/KillMethods/kill-method-specific-melee.json');
        $innerConditionJson = str_replace('{{PARAM-WEAPON-GUID}}', $weaponMapping->getIoiGuid(), $innerConditionJson);

        return $innerConditionJson;
    }

    private function transformKillMethodBroad(SpinResult $result): string {
        $file = 'killmethodbroad';
        if ($result->killMethod->ioiMethod === 'pistol') {
            $file = 'pistol';
        } elseif ($result->killMethod->ioiMethod === 'smg') {
            $file = 'smg';
        }

        $innerConditionJson = file_get_contents(__DIR__ . "/Missions/MissionJson/Partial/KillMethods/kill-method-{$file}.json");
        $innerConditionJson = str_replace('{{BROAD-KILL-METHOD}}', $result->killMethod->ioiMethod, $innerConditionJson);

        $selectedVariant = $this->spinTransformerHelpers->getChosenVariant($result->killMethod->variants);
        $variantJson = file_get_contents(__DIR__ . "/Missions/MissionJson/Partial/KillMethods/kill-method-variant.json");
        if ($selectedVariant !== null) {
            if ($result->killMethod->ioiMethod === 'explosive') {
                $variantJson = file_get_contents(__DIR__ . "/Missions/MissionJson/Partial/KillMethods/kill-method-variant-loud-explosive-weapon.json");
            } else {
                $variantJson = str_replace('{{KILL-METHOD-VARIANT-KEY}}', '$Value.WeaponSilenced', $variantJson);
                $variantJson = str_replace('"{{KILL-METHOD-VARIANT-VALUE}}"',
                    $selectedVariant->name === 'Loud' ? 'false' : 'true',
                    $variantJson);
            }
            $innerConditionJson = str_replace('"{{KILL-METHOD-VARIANT}}"', $variantJson, $innerConditionJson);
        } else {
            $innerConditionJson = str_replace('"{{KILL-METHOD-VARIANT}}",', '', $innerConditionJson);
        }

        return $innerConditionJson;
    }

    private function getAdditionalDisguiseGuidsForDisguise(DisguiseRestriction $disguiseRestriction) {
        switch ($disguiseRestriction->name) {
            case 'Club Security':
                return [
                    '6ca35f8b-b244-44a0-9813-dc050a565ac2',
                    'acb7695a-a5eb-420a-8455-d409d08d53e2',
                    'd9e1d24c-c9cc-48d6-bfd4-821d3e742b64'
                ];
            case 'Technician':
                return ['61545feb-9594-4231-8fa2-f98307ac796f'];
            case 'Biker':
                return [
                    '034d5a11-1e5c-4f57-99d9-233443e42caf',
                    'd222f4c4-708a-42c7-9433-f8c5cfd72706',
                    'e65fb964-a5e3-45b6-99d6-75d3c539ae92'
                ];
            case 'Club Crew':
                return ['107c35d7-7300-417c-832a-4f36cd3071b9'];
            default:
                return [];
        }
    }
}