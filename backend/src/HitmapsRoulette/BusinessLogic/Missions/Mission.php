<?php


namespace HitmapsRoulette\BusinessLogic\Missions;


use HitmapsRoulette\BusinessLogic\KillMethod;

class Mission {
    public $slug;
    public $name;
    public $locationTileUrl;

    /* @var $targets Target[] */
    public $targets;
    public $melee;
}
