<?php


namespace HitmapsRoulette\BusinessLogic\Missions;


use Exception;
use HitmapsRoulette\BusinessLogic\BlacklistedKill;
use HitmapsRoulette\BusinessLogic\KillMethod;

class MissionRetriever {
    private $missionJson = [];

    public function __construct() {
        $this->missionJson = json_decode(file_get_contents(__DIR__ . '/missions.json'), true);
    }

    function getMissionInfo($missionSlug): Mission {
        $missionInfoJson = array_values(array_filter($this->missionJson, function($el) use ($missionSlug) {
            return $el['slug'] === $missionSlug;
        }));
        if (count($missionInfoJson) !== 1) {
            throw new Exception("Did not find exactly 1 mission for slug '{$missionSlug}'.  Found " . count($missionInfoJson) . " instead.");
        }

        $mission = new Mission();
        $mission->name = $missionInfoJson[0]['name'];
        $mission->slug = $missionSlug;
        $mission->locationTileUrl = $missionInfoJson[0]['locationTileUrl'];
        $mission->targets = $this->transformTargets($missionInfoJson[0]['targets']);
        $mission->melee = $this->transformMelee($missionInfoJson[0]['melee']);

        return $mission;
    }

    //region Target transformation
    private function transformTargets(array $targets): array {
        $transformed = [];
        foreach ($targets as $target) {
            $transformedTarget = new Target(
                $target['id'],
                $target['name'],
                $target['tileUrl'],
                $this->transformKills($target['uniqueKills']),
                $this->transformBlacklistedKills($target['blacklistedKills'])
            );

            if (isset($target['killOverrides'])) {
                $transformedTarget->killOverrides = $this->transformKillsOverrides($target['killOverrides']);
            }

            $transformed[] = $transformedTarget;
        }

        return $transformed;
    }

    private function transformKills(array $uniqueKills): array {
        $transformed = [];
        foreach ($uniqueKills as $uniqueKill) {
            $transformed[] = new KillMethod(
                $uniqueKill['name'],
                'unique',
                $uniqueKill['tileUrl']
            );
        }

        return $transformed;
    }

    private function transformKillsOverrides(array $killOverrides): array {
        $transformed = [];
        foreach ($killOverrides as $killOverride) {
            $transformed[] = new KillMethod(
                $killOverride['name'],
                $killOverride['ioiMethod'],
                $killOverride['tileUrl']
            );
        }

        return $transformed;
    }

    private function transformBlacklistedKills(array $blacklistedKills): array {
        $transformed = [];
        foreach ($blacklistedKills as $blacklistedKill) {
            $transformed[] = new BlacklistedKill(
                $blacklistedKill['killName'],
                $blacklistedKill['disguise']
            );
        }

        return $transformed;
    }
    //endregion

    private function transformMelee(array $melees): array {
        $transformed = [];
        foreach ($melees as $melee) {
            $transformed[] = new KillMethod(
                $melee['name'],
                $this->getIoiMethodForMelee($melee['name']),
                $melee['tileUrl']
            );
        }

        return $transformed;
    }

    private function getIoiMethodForMelee(string $melee) {
        switch ($melee) {
            case 'Neck Snap':
                return 'unarmed';
            case 'Fiber Wire':
                return 'fiberwire';
            default:
                return 'unknown';
        }
    }
}
