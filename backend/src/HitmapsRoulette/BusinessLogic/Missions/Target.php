<?php


namespace HitmapsRoulette\BusinessLogic\Missions;


use HitmapsRoulette\BusinessLogic\BlacklistedKill;
use HitmapsRoulette\BusinessLogic\KillMethod;

class Target {
    public $id;
    public $name;
    public $tileUrl;
    public $uniqueKills = [];
    public $blacklistedKills = [];
    public $killOverrides = [];

    /**
     * Target constructor.
     * @param $id
     * @param $name
     * @param $tileUrl
     * @param KillMethod[] $uniqueKills
     * @param BlacklistedKill[] $blacklistedKills
     */
    public function __construct(int $id, string $name, string $tileUrl, array $uniqueKills, array $blacklistedKills) {
        $this->id = $id;
        $this->name = $name;
        $this->tileUrl = $tileUrl;
        $this->uniqueKills = $uniqueKills;
        $this->blacklistedKills = $blacklistedKills;
    }


}
