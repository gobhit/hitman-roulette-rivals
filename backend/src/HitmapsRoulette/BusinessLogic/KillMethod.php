<?php


namespace HitmapsRoulette\BusinessLogic;


use HitmapsRoulette\BusinessLogic\CommonKills\CommonKillVariant;

class KillMethod {
    public $name;
    public $ioiMethod;
    public $tileUrl;

    /* @var $variants CommonKillVariant[] */
    public $variants;
    public $chosen;

    public function __construct(string $name, ?string $ioiMethod, ?string $tileUrl, array $variants = []) {
        $this->name = $name;
        $this->ioiMethod = $ioiMethod;
        $this->tileUrl = $tileUrl;
        $this->variants = $variants;
        $this->chosen = false;
    }
}
