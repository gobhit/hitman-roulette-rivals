<?php


namespace HitmapsRoulette\BusinessLogic\CommonKills;

use HitmapsRoulette\BusinessLogic\KillMethod;

// TODO Make this database-driven eventually
class CommonKillsRetriever {
    public function getFirearms() {
        return [
            new KillMethod('Pistol',
                'pistol',
                'https://media.hitmaps.com/img/hitman3/contractconditions/condition_killmethod_pistol.jpg',
                [new CommonKillVariant('Loud'), new CommonKillVariant('Silenced')]),
            new KillMethod('Sniper Rifle',
                'sniperrifle',
                'https://media.hitmaps.com/img/hitman3/contractconditions/condition_killmethod_sniperrifle.jpg',
                [new CommonKillVariant('Loud'), new CommonKillVariant('Silenced')]),
            new KillMethod('Explosive (Weapon)',
                'explosive',
                'https://media.hitmaps.com/img/hitman3/contractconditions/condition_killmethod_explosive.jpg',
                [new CommonKillVariant('Loud')]),
            new KillMethod('Assault Rifle',
                'assaultrifle',
                'https://media.hitmaps.com/img/hitman3/contractconditions/condition_killmethod_assaultrifle.jpg',
                [new CommonKillVariant('Loud'), new CommonKillVariant('Silenced')]),
            new KillMethod('SMG',
                'smg',
                'https://media.hitmaps.com/img/hitman3/contractconditions/condition_killmethod_smg.jpg',
                [new CommonKillVariant('Loud'), new CommonKillVariant('Silenced')]),
            new KillMethod('Shotgun',
                'shotgun',
                'https://media.hitmaps.com/img/hitman3/contractconditions/condition_killmethod_shotgun.jpg',
                [new CommonKillVariant('Loud'), new CommonKillVariant('Silenced')]),
        ];
    }

    public function getAccidents() {
        return [
            new KillMethod('Drowning',
                'accident_drown',
                'https://media.hitmaps.com/img/hitman3/contractconditions/condition_killmethod_accident_drown.jpg'),
            new KillMethod('Falling Object',
                'accident_suspended_object',
                'https://media.hitmaps.com/img/hitman3/contractconditions/condition_killmethod_accident_suspended_object.jpg'),
            new KillMethod('Fall',
                'accident_push',
                'https://media.hitmaps.com/img/hitman3/contractconditions/condition_killmethod_accident_push.jpg'),
            new KillMethod('Fire',
                'accident_burn',
                'https://media.hitmaps.com/img/hitman3/contractconditions/condition_killmethod_accident_burn.jpg'),
            new KillMethod('Electrocution',
                'accident_electric',
                'https://media.hitmaps.com/img/hitman3/contractconditions/condition_killmethod_accident_electric.jpg'),
            new KillMethod('Explosion (Accident)',
                'accident_explosion',
                'https://media.hitmaps.com/img/hitman3/contractconditions/condition_killmethod_accident_explosion.jpg'),
            new KillMethod('Consumed Poison',
                'consumed_poison',
                'https://media.hitmaps.com/img/hitman3/contractconditions/condition_killmethod_consumed_poison.jpg'),
            new KillMethod('Injected Poison',
                'injected_poison',
                'https://media.hitmaps.com/img/hitman3/contractconditions/condition_killmethod_injected_poison.jpg'),
        ];
    }

    public function getGenericKills() {
        return [
            new KillMethod('Firearm',
                'unknown',
                'https://media.hitmaps.com/img/hitman3/contractconditions/condition_killmethod_firearm.jpg'),
            new KillMethod('Concealable Melee',
                'unknown',
                'https://media.hitmaps.com/img/hitman3/contractconditions/condition_killmethod_throw.jpg'),
            new KillMethod('Non-concealable Melee',
                'unknown',
                'https://media.hitmaps.com/img/hitman3/contractconditions/condition_killmethod_melee_lethal.jpg'),
            new KillMethod('Accident',
                'accident',
                'https://media.hitmaps.com/img/hitman3/contractconditions/condition_killmethod_accident.jpg'),
            new KillMethod('Explosion',
                'explosion-either-or',
                'https://media.hitmaps.com/img/hitman3/contractconditions/condition_killmethod_explosion.jpg'),
            new KillMethod('Poison',
                'poison',
                'https://media.hitmaps.com/img/hitman3/contractconditions/condition_killmethod_poison.jpg')
        ];
    }
}
