<?php


namespace HitmapsRoulette\BusinessLogic\CommonKills;


class CommonKillVariant {
    public $name;
    public $chosen;

    public function __construct($name, $chosen = false) {
        $this->name = $name;
        $this->chosen = $chosen;
    }
}