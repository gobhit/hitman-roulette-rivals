<?php

namespace HitmapsRoulette\BusinessLogic;


class RandomHandler {
    public static function &getRandomElementFromArray(array &$array) {
        if (count($array) === 0) {
            return null;
        }

        return $array[random_int(0, count($array) - 1)];
    }

    public static function isRngInYourFavor(int $min, int $max, int $expected): bool {
        return random_int($min, $max) === $expected;
    }
}