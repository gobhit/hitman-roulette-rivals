<?php

namespace HitmapsRoulette\BusinessLogic;


use HitmapsRoulette\BusinessLogic\Missions\Target;

class SpinResult {
    public $target;
    public $killMethod;
    public $disguise;

    public function __construct(Target $target, KillMethod $killMethod, DisguiseRestriction $disguise) {
        $this->target = $target;
        $this->killMethod = $killMethod;
        $this->disguise = $disguise;
    }
}