<?php


namespace HitmapsRoulette\BusinessLogic;


class DisguiseRestriction {
    public $name;
    public $tileUrl;
    public $chosen = false;

    public function __construct(string $name, string $tileUrl) {
        $this->name = $name;
        $this->tileUrl = $tileUrl;
    }
}
