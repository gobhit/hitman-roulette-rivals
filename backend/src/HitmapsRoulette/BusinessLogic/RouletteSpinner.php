<?php


namespace HitmapsRoulette\BusinessLogic;


use Exception;
use HitmapsRoulette\BusinessLogic\CommonKills\CommonKillsRetriever;
use HitmapsRoulette\BusinessLogic\CommonKills\CommonKillVariant;
use HitmapsRoulette\BusinessLogic\Missions\MissionRetriever;
use Predis\Client;

class RouletteSpinner {
    private $missionRetriever;
    private $commonKillsRetriever;
    private $predis;
    private $httpClient;

    public function __construct(MissionRetriever $missionRetriever,
                                CommonKillsRetriever $commonKillsRetriever,
                                Client $predis,
                                \GuzzleHttp\Client $httpClient) {
        $this->missionRetriever = $missionRetriever;
        $this->commonKillsRetriever = $commonKillsRetriever;
        $this->predis = $predis;
        $this->httpClient = $httpClient;
    }

    public function spin(array $missionPool, CriteriaFilters $criteriaFilters, bool $forGameClient): GeneratedSpin {
        // 1. Pick mission to spin
        $selectedMission = $missionPool[array_rand($missionPool)];

        // 2. Get mission info from Redis if available, or use HITMAPS API to build the necessary info
        // mission info = {game}|{location}|{mission}|{difficulty}
        $gameInformation = explode('|', $selectedMission);
        $hitmapsMissionInfo = $this->getWeaponAndDisguiseInfoFromHitmaps($gameInformation[0], $gameInformation[1], $gameInformation[2], $gameInformation[3]);
        $missionInfo = $this->missionRetriever->getMissionInfo($gameInformation[2]);
        $possibleKills = $this->buildPossibleKills($hitmapsMissionInfo, $missionInfo, $criteriaFilters);

        $targetConditions = [];
        foreach ($missionInfo->targets as $target) {
            $blacklistedKillNames = [];
            foreach ($target->blacklistedKills as $blacklistedKill) {
                /* @var $blacklistedKill BlacklistedKill */
                if ($blacklistedKill->disguise === '*') {
                    $blacklistedKillNames[] = $blacklistedKill->killName;
                }
            }

            //region KillMethod
            /* @var $selectedKill KillMethod|null */
            $selectedKill = null;
            if (count($target->killOverrides)) {
                if ($forGameClient) {
                    $gameCompatibleKills = array_values(array_filter($target->killOverrides, function($element) {
                        return $element->ioiMethod !== null;
                    }));
                    $selectedKill = RandomHandler::getRandomElementFromArray($gameCompatibleKills);
                } else {
                    $selectedKill = RandomHandler::getRandomElementFromArray($target->killOverrides);
                }
            } else {
                $availableKills = $this->getAvailableKills($possibleKills, $blacklistedKillNames);

                /* @var $killCategory KillMethod[] */
                $killCategory = RandomHandler::getRandomElementFromArray($availableKills);
                $selectedKill = RandomHandler::getRandomElementFromArray($killCategory);
            }

            $selectedKill->chosen = true;
            if (count($selectedKill->variants) > 0 &&
                $target->name !== 'Arthur Edwards' &&
                !($target->name === 'Sierra Knox' && $selectedKill->ioiMethod === 'sniperrifle') &&
                RandomHandler::isRngInYourFavor(1, 2, 2)) {
                /* @var $variant CommonKillVariant */
                $variant = RandomHandler::getRandomElementFromArray($selectedKill->variants);
                $variant->chosen = true;
            }
            //endregion

            //region Disguise
            $selectedDisguise = null;
            if ($criteriaFilters->specificDisguises) {
                $attempt = 0;
                do {
                    /* @var $selectedDisguise &DisguiseRestriction */
                    $selectedDisguise = RandomHandler::getRandomElementFromArray($hitmapsMissionInfo->disguises);
                    $attempt++;
                } while ($attempt < 100 && $this->isDisguiseProhibited($selectedDisguise, $selectedKill, $target->blacklistedKills));

                if ($attempt === 100) {
                    throw new Exception("Bad configuration for kill method: {$selectedKill->name}!");
                }
                $selectedDisguise->chosen = true;
            } else {
                $selectedDisguise = new DisguiseRestriction('Any Disguise', '');
            }

            //endregion

            $targetConditions[] = new SpinResult($target, $selectedKill, $selectedDisguise);
        }

        //--> Let's do the spin
        $generatedSpin = new GeneratedSpin();
        $generatedSpin->mission = $missionInfo;
        $generatedSpin->targetConditions = $targetConditions;

        return $generatedSpin;
    }

    private function getWeaponAndDisguiseInfoFromHitmaps(string $game, string $location, string $mission, string $difficulty): HitmapsMissionInfo {
        $cacheKey = "roulette:{$game}|{$location}|{$mission}|{$difficulty}";
        $cachedInfo = $this->predis->get($cacheKey);
        if ($cachedInfo !== null) {
            return unserialize($cachedInfo);
        }

        $url = "https://www.hitmaps.com/api/v1/games/{$game}/locations/{$location}/missions/{$mission}/{$difficulty}/map";
        $response = $this->httpClient->get($url);
        if ($response->getStatusCode() !== 200) {
            throw new Exception("Received non-success status from HITMAPS: {$response->getStatusCode()}");
        }

        $responseJson = json_decode($response->getBody(), true);
        $hitmapsMissionInfo = new HitmapsMissionInfo();
        foreach ($responseJson['searchableNodes']['Weapons and Tools']['items']['Lethal Melee']['items'] as $item) {
            // Ignoring fiber wire variants
            if (in_array($item['name'], ['Earphones', 'Measuring Tape', 'Stethoscope', 'Aztec Necklace', 'Fishing Line']) !== false) {
                continue;
            }

            $hitmapsMissionInfo->lethalMelee[] = new KillMethod($item['name'], 'killmethod_specificitem', $item['image']);
        }

        $foundASuit = false;
        foreach ($responseJson['disguises'] as $disguise) {
            if ($disguise['suit'] && $foundASuit) {
                continue;
            }

            if ($disguise['suit']) {
                $foundASuit = true;
            }

            $name = $disguise['suit'] ? 'Suit' : $disguise['name'];
            $hitmapsMissionInfo->disguises[] = new DisguiseRestriction($name, $disguise['image']);
        }

        $this->predis->set($cacheKey, serialize($hitmapsMissionInfo));

        return $hitmapsMissionInfo;
    }

    private function buildPossibleKills(HitmapsMissionInfo $hitmapsMissionInfo, Missions\Mission $missionInfo, CriteriaFilters $criteriaFilters): array {
        $availableKills = [];
        if ($criteriaFilters->genericKills) {
            $availableKills[] = $this->commonKillsRetriever->getGenericKills();
        }
        if ($criteriaFilters->specificFirearms) {
            $availableKills[] = $this->commonKillsRetriever->getFirearms();
        }
        if ($criteriaFilters->specificAccidents) {
            $availableKills[] = $this->commonKillsRetriever->getAccidents();
        }
        if ($criteriaFilters->specificMelee) {
            $allMelee = $missionInfo->melee;
            foreach ($hitmapsMissionInfo->lethalMelee as $melee) {
                $allMelee[] = $melee;
            }

            $availableKills[] = $allMelee;
        }

        return $availableKills;
    }

    private function getAvailableKills(array &$possibleKills, array &$blacklistedKillNames): array {
        $actuallyPossibleKills = [];
        foreach ($possibleKills as &$killCategory) {
            $actuallyPossibleForCategory = [];
            /* @var $killMethod KillMethod */
            foreach ($killCategory as &$killMethod) {
                if (!$killMethod->chosen &&
                    !in_array($killMethod->name, $blacklistedKillNames)) {
                    $actuallyPossibleForCategory[] = $killMethod;
                }
            }

            $actuallyPossibleKills[] = $actuallyPossibleForCategory;
        }

        return $actuallyPossibleKills;
    }

    private function isDisguiseProhibited(DisguiseRestriction &$disguiseRestriction, KillMethod &$killMethod, array &$blacklistedKills): bool {
        if ($disguiseRestriction->chosen === true) {
            return true;
        }

        $blacklistedKill = array_filter($blacklistedKills, fn($blacklistedKill) => $blacklistedKill->name === $killMethod->name);

        foreach ($blacklistedKill as $disguiseRestrictable) {
            if ($disguiseRestriction->name === $disguiseRestrictable->disguise) {
                return true;
            }
        }

        return false;
    }
}
