<?php


namespace HitmapsRoulette\BusinessLogic;


class CriteriaFilters {
    public $specificDisguises = true;
    public $specificMelee = true;
    public $specificFirearms = true;
    public $specificAccidents = true;
    public $uniqueTargetKills = false;
    public $genericKills = false;
    public $rrBannedKills = false;

    public static function fromJson(array $json): CriteriaFilters {
        $criteriaFilters = new CriteriaFilters();
        $criteriaFilters->specificDisguises = self::getProperty($json, 'specificDisguises', $criteriaFilters->specificDisguises);
        $criteriaFilters->specificMelee = self::getProperty($json, 'specificMelee', $criteriaFilters->specificMelee);
        $criteriaFilters->specificFirearms = self::getProperty($json, 'specificFirearms', $criteriaFilters->specificFirearms);
        $criteriaFilters->specificAccidents = self::getProperty($json, 'specificAccidents', $criteriaFilters->specificAccidents);
        $criteriaFilters->uniqueTargetKills = self::getProperty($json, 'uniqueTargetKills', $criteriaFilters->uniqueTargetKills);
        $criteriaFilters->genericKills = self::getProperty($json, 'genericKills', $criteriaFilters->genericKills);
        $criteriaFilters->rrBannedKills = self::getProperty($json, 'rrBannedKills', $criteriaFilters->rrBannedKills);

        return $criteriaFilters;
    }

    private static function getProperty(array $json, string $key, bool $default = false) {
        return isset($json[$key]) ?
            $json[$key] :
            $default;
    }
}