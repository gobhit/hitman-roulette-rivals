<?php


namespace HitmapsRoulette\BusinessLogic;


class ErichSodersSpinTransformer {
    public function transform(string $guid, SpinResult $result, ?string $disguiseGuid): string {
        $json = file_get_contents(__DIR__ . '/Missions/MissionJson/Partial/target-Erich-Soders.json');

        $json = str_replace('{{TARGET-OBJECTIVE-GUID}}', $guid, $json);
        $json = str_replace('{{PARAM-KILL-METHOD-NAME}}', $result->killMethod->name, $json);
        $json = str_replace('{{PARAM-DISGUISE-NAME}}', str_replace('"', '\\"', $result->disguise->name), $json);

        // Remove this condition from the banned list
        $json = str_replace('"'. $result->killMethod->ioiMethod . '",', '', $json);

        // Now set it for the real objective
        $json = str_replace('{{METRIC-KILL-NAME}}', $result->killMethod->ioiMethod, $json);

        if ($result->disguise->name === 'Any Disguise') {
            $json = str_replace(' while wearing <b>{{PARAM-DISGUISE-NAME-SUFFIX}}</b>', '', $json);
            $json = str_replace('"{{DISGUISE-RESTRICTION-STATEMACHINE}}",', '', $json);

            $negateDisguiseStateMachine = file_get_contents(__DIR__ . "/Missions/MissionJson/Partial/Disguise/disguise-negate-Erich-Soders-any.json");
            $json = str_replace('"{{NEGATED-DISGUISE-RESTRICTION-STATEMACHINE}}",', $negateDisguiseStateMachine, $json);
        } else {
            $suit = $result->disguise->name === 'Suit';
            $escapedDisguise = str_replace('"', '\\"', $result->disguise->name);
            $suffix = $suit ? 'a Suit' : "disguise: {$escapedDisguise}";
            $json = str_replace('{{PARAM-DISGUISE-NAME-SUFFIX}}', $suffix, $json);

            $disguiseStateMachineFilename = $suit ? 'suit' : 'specific';
            $disguiseStateMachine = file_get_contents(__DIR__ . "/Missions/MissionJson/Partial/Disguise/disguise-Erich-Soders-{$disguiseStateMachineFilename}.json");

            if (!$suit) {
                $disguiseStateMachine = str_replace('{{PARAM-DISGUISE-GUID}}', $disguiseGuid, $disguiseStateMachine);
            }
            $json = str_replace('"{{DISGUISE-RESTRICTION-STATEMACHINE}}"', $disguiseStateMachine, $json);

        }

        return $json;
    }
}