<?php


namespace HitmapsRoulette\BusinessLogic;


use HitmapsRoulette\BusinessLogic\Missions\Mission;

class GeneratedSpin {
    /* @var $mission Mission */
    public $mission;

    /* @var $targetConditions SpinResult[] */
    public $targetConditions;

    // TODO For future endeavors
    public $requiredEntrance;
    public $requiredExit;
    public $aditionalObjectives;
    public $complications;
}