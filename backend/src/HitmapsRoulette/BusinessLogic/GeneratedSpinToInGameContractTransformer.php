<?php


namespace HitmapsRoulette\BusinessLogic;


use Doctrine\ORM\EntityManager;
use Exception;
use HitmapsRoulette\BusinessLogic\Missions\Mission;
use HitmapsRoulette\DataAccess\Models\DisguiseToIoiGuid;

class GeneratedSpinToInGameContractTransformer {
    private $entityManager;
    private $erichSodersSpinTransformer;
    private $apexPredatorAgentTransformer;
    private $spinTransformerHelpers;

    public function __construct(EntityManager $entityManager,
                                ErichSodersSpinTransformer $erichSodersSpinTransformer,
                                ApexPredatorAgentTransformer $apexPredatorAgentTransformer,
                                SpinTransformerHelpers $spinTransformerHelpers) {
        $this->entityManager = $entityManager;
        $this->erichSodersSpinTransformer = $erichSodersSpinTransformer;
        $this->apexPredatorAgentTransformer = $apexPredatorAgentTransformer;
        $this->spinTransformerHelpers = $spinTransformerHelpers;
    }

    /**
     * Returns an game-friendly contract
     *
     * @param GeneratedSpin $generatedSpin
     * @return array
     * @throws Exception When unable to find the mission JSON
     */
    public function transform(GeneratedSpin $generatedSpin): array {
        $missionJson = file_get_contents(__DIR__ . "/Missions/MissionJson/{$generatedSpin->mission->slug}.json");

        if ($missionJson === false) {
            throw new Exception("Unable to find mission JSON for mission: {$generatedSpin->mission->name}!");
        }

        // Contract Information
        $contractId = $this->generateGuid();
        $publicId = "001000000047";
        $missionJson = str_replace('{{PARAM-PUBLIC-ID}}', $publicId, $missionJson);
        $missionJson = str_replace('{{PARAM-CONTRACT-ID}}', $contractId, $missionJson);

        $decodedMissionJson = json_decode($missionJson, true);

        $i = 0;
        foreach ($generatedSpin->targetConditions as $spinResult) {
            $targetGuid = str_replace('|TARGET', '', $decodedMissionJson['Data']['Objectives'][$i]);
            $hasDisguise = $spinResult->disguise->name !== 'Any Disguise';
            $targetObjectiveGuid = $this->generateGuid();

            if ($spinResult->target->name === 'Erich Soders') {
                $disguiseGuid = null;
                if ($spinResult->disguise->name !== 'Suit') {
                    $disguiseGuid = $this->getDisguiseGuid($generatedSpin->mission, $spinResult->disguise->name);
                }

                $result = $this->erichSodersSpinTransformer->transform($targetObjectiveGuid, $spinResult, $disguiseGuid);

                $missionJson = str_replace('"'.$targetGuid.'|TARGET"', $result, $missionJson);

                // GroupObjectiveDisplayOrder
                $missionJson = str_replace("{$targetGuid}|TARGET-GUID", $targetObjectiveGuid, $missionJson);

                $i++;
                continue;
            }

            if ($generatedSpin->mission->slug === 'apex-predator') {
                $disguiseGuid = null;
                if ($spinResult->disguise->name !== 'Suit' && $spinResult->disguise->name !== 'Any Disguise') {
                    $disguiseGuid = $this->getDisguiseGuid($generatedSpin->mission, $spinResult->disguise->name);
                }

                $result = $this->apexPredatorAgentTransformer->transform($targetObjectiveGuid, $spinResult, $disguiseGuid);

                $missionJson = str_replace('"'.$targetGuid.'|TARGET"', $result->targetJson, $missionJson);

                //region Failure check
                $agentNumber = $i + 1;
                $missionJson = str_replace('"{{AGENT-'.$agentNumber.'-KILL-CONDITION}}"', $result->killCondition, $missionJson);

                if ($result->disguiseRestriction !== 'REMOVE') {
                    $missionJson = str_replace('"{{AGENT-'.$agentNumber.'-DISGUISE-CONDITION}}"', $result->disguiseRestriction, $missionJson);
                } else {
                    $missionJson = str_replace('"{{AGENT-'.$agentNumber.'-DISGUISE-CONDITION}}",', '', $missionJson);
                }

                //endregion

                $i++;
                continue;
            }


            //region Disguise
            $disguiseTargetConditionsJson = 'REMOVE';
            $disguiseStateMachineJson = 'REMOVE';
            $disguiseGuid = $this->generateGuid();
            if ($hasDisguise) {
                $suit = $spinResult->disguise->name === 'Suit';
                $disguiseIoiGuid = $this->getDisguiseGuid($generatedSpin->mission, $spinResult->disguise->name);
                //region TargetConditions
                $targetConditionsName = $suit ? 'disguise-suit.json' : 'disguise-specific.json';
                $disguiseTargetConditionsJson = file_get_contents(__DIR__ . "/Missions/MissionJson/Partial/TargetConditions/{$targetConditionsName}");
                $disguiseTargetConditionsJson = str_replace('{{DISGUISE-GUID}}',
                    $disguiseIoiGuid,
                    $disguiseTargetConditionsJson);
                $disguiseTargetConditionsJson = str_replace('{{DISGUISE-OBJECTIVE-GUID}}', $disguiseGuid, $disguiseTargetConditionsJson);
                //endregion
                //region statemachine
                $disguiseFilename = $spinResult->target->name === 'Sierra Knox' ? 'disguise-sierra-knox' : 'disguise';
                $innerDisguiseFilename = $spinResult->disguise->name === 'Any Disguise' ? 'disguise-any' : 'disguise-specific';
                $disguiseStateMachineJson = file_get_contents(__DIR__ . "/Missions/MissionJson/Partial/{$disguiseFilename}.json");
                $innerDisguiseStateMachineJson = file_get_contents(__DIR__ . "/Missions/MissionJson/Partial/Disguise/{$innerDisguiseFilename}.json");
                $disguiseStateMachineJson = str_replace('{{PARAM-TARGET-NAME}}', $spinResult->target->name, $disguiseStateMachineJson);
                $disguiseStateMachineJson = str_replace('{{PARAM-DISGUISE-NAME}}', str_replace('"', '\\"', $spinResult->disguise->name), $disguiseStateMachineJson);
                $disguiseStateMachineJson = str_replace('{{DISGUISE-OBJECTIVE-GUID}}', $disguiseGuid, $disguiseStateMachineJson);
                $disguiseStateMachineJson = str_replace('{{PARAM-TARGET-GUID}}', $targetGuid, $disguiseStateMachineJson);
                $innerDisguiseStateMachineJson = str_replace('{{DISGUISE-PROPERTY-NAME}}',
                    ($suit ? '$Value.OutfitIsHitmanSuit' : '$Value.OutfitRepositoryId'),
                    $innerDisguiseStateMachineJson);

                $additionalDisguiseGuids = [];
                if (!$suit) {
                    $additionalDisguiseGuids = $this->getAdditionalDisguiseGuidsForDisguise($disguiseIoiGuid);
                }
                $formattedDisguiseGuids = $disguiseIoiGuid;
                foreach ($additionalDisguiseGuids as $additionalDisguiseGuid) {
                    $formattedDisguiseGuids .= '","' . $additionalDisguiseGuid;
                }
                $innerDisguiseStateMachineJson = str_replace(($suit ? '"{{DISGUISE-PROPERTY-VALUE}}"' : '{{DISGUISE-PROPERTY-VALUE}}'),
                    ($suit ? 'true' : $formattedDisguiseGuids),
                    $innerDisguiseStateMachineJson);
                $disguiseStateMachineJson = str_replace('"{{INNER-DISGUISE-METHOD-JSON}}"', $innerDisguiseStateMachineJson, $disguiseStateMachineJson);
                //endregion
            }
            //endregion

            //region KillMethod
            $weaponConditionGuid = $this->generateGuid();
            if (str_contains($spinResult->killMethod->ioiMethod, 'accident_') ||
                in_array($spinResult->killMethod->ioiMethod, ['injected_poison', 'consumed_poison'])) {
                $weaponTargetConditionsJson = $this->spinTransformerHelpers->transformKillMethodStrict($targetGuid, $spinResult, $weaponConditionGuid, 'TargetCondition');
                $weaponStateMachineJson = $this->spinTransformerHelpers->transformKillMethodStrict($targetGuid, $spinResult, $weaponConditionGuid, 'StateMachine');
            } elseif ($spinResult->killMethod->ioiMethod === 'killmethod_specificitem') {
                $weaponTargetConditionsJson = $this->spinTransformerHelpers->transformSpecificMelee($targetGuid, $spinResult, $weaponConditionGuid, 'TargetCondition');
                $weaponStateMachineJson = $this->spinTransformerHelpers->transformSpecificMelee($targetGuid, $spinResult, $weaponConditionGuid, 'StateMachine');
            } elseif ($this->spinTransformerHelpers->isKillMethodBroad($spinResult->killMethod->ioiMethod)) {
                $weaponTargetConditionsJson = $this->spinTransformerHelpers->transformKillMethodBroad($targetGuid, $spinResult, $weaponConditionGuid, 'TargetCondition');
                $weaponStateMachineJson = $this->spinTransformerHelpers->transformKillMethodBroad($targetGuid, $spinResult, $weaponConditionGuid, 'StateMachine');
            } else {
                throw new Exception("Unable to handle objective kill method: '{$spinResult->killMethod->ioiMethod}'");
            }
            //endregion

            //region Target
            $targetSpecificJsonName = 'target-'.str_replace(' ', '-', $spinResult->target->name).'.json';
            if (file_exists(__DIR__ . "/Missions/MissionJson/Partial/{$targetSpecificJsonName}")) {
                $targetJsonName = $targetSpecificJsonName;
            } else {
                $targetJsonName = 'target.json';
            }
            $targetJson = file_get_contents(__DIR__ . "/Missions/MissionJson/Partial/{$targetJsonName}");
            $targetJson = str_replace('{{TARGET-OBJECTIVE-GUID}}', $targetObjectiveGuid, $targetJson);

            $variant = $this->spinTransformerHelpers->getChosenVariant($spinResult->killMethod->variants);
            if ($variant === null) {
                $targetJson = str_replace('"ObjectiveType": "customkill",', '', $targetJson);
            }
            //endregion
            //region Put it all together
            if ($hasDisguise) {
                $targetJson = str_replace('"{{TARGET-CONDITIONS-DISGUISE}}"', $disguiseTargetConditionsJson, $targetJson);
                $targetJson = str_replace('{{PARAM-DISGUISE-NAME}}', str_replace('"', '\\"', $spinResult->disguise->name), $targetJson);

                $escapedDisguiseName = str_replace('"', '\\"', $spinResult->disguise->name);
                $disguiseNameSuffix = $spinResult->disguise->name === 'Suit' ? 'a Suit' : "disguise: {$escapedDisguiseName}";
                $targetJson = str_replace('{{PARAM-DISGUISE-NAME-SUFFIX}}', $disguiseNameSuffix, $targetJson);
            } else {
                $targetJson = str_replace('"{{TARGET-CONDITIONS-DISGUISE}}",', '', $targetJson);
                $targetJson = str_replace(' / {{PARAM-DISGUISE-NAME}}', '', $targetJson);
                $targetJson = str_replace(' while wearing <b>{{PARAM-DISGUISE-NAME-SUFFIX}}</b>', '', $targetJson);
                $targetJson = str_replace('\t\t\t\t\tWear Disguise: <b>{{PARAM-DISGUISE-NAME}}</b>', '', $targetJson);
            }

            $targetJson = str_replace('"{{TARGET-CONDITIONS-WEAPON}}"', $weaponTargetConditionsJson, $targetJson);
            $targetJson = str_replace('{{PARAM-TARGET-NAME}}', $spinResult->target->name, $targetJson);
            $killMethodName = $variant === null ?
                $spinResult->killMethod->name :
                "{$variant->name} {$spinResult->killMethod->name}";
            $targetJson = str_replace('{{PARAM-KILL-METHOD-NAME}}', $killMethodName, $targetJson);


            $targetJson = str_replace('{{PARAM-TARGET-GUID}}', $targetGuid, $targetJson);

            $missionJson = str_replace('"'.$targetGuid.'|TARGET"', $targetJson, $missionJson);
            $missionJson = str_replace('"'.$targetGuid.'|KILL-STATE-MACHINE"', $weaponStateMachineJson, $missionJson);

            if ($hasDisguise) {
                $missionJson = str_replace('"'.$targetGuid.'|DISGUISE-STATE-MACHINE"', $disguiseStateMachineJson, $missionJson);
            } else {
                $missionJson = str_replace('"'.$targetGuid.'|DISGUISE-STATE-MACHINE",', '', $missionJson);
            }

            // GroupObjectiveDisplayOrder
            $missionJson = str_replace("{$targetGuid}|TARGET-GUID", $targetObjectiveGuid, $missionJson);
            if ($hasDisguise) {
                $missionJson = str_replace("{$targetGuid}|DISGUISE-GUID", $disguiseGuid, $missionJson);
            } else {
                $missionJson = str_replace('{"Id": "'.$targetGuid.'|DISGUISE-GUID"},', '', $missionJson);
            }
            $missionJson = str_replace("{$targetGuid}|KILL-GUID", $weaponConditionGuid, $missionJson);
            //endregion

            $i += 3;
        }

        $decodedJson = json_decode($missionJson, true);
        if ($decodedJson === null) {
            var_dump($missionJson);
            return [];
        }

        return $decodedJson;
    }

    private function getDisguiseGuid(Mission $mission, string $disguiseName): string {
        if ($disguiseName === 'Suit') {
            return '';
        }

        $missionInfo = $this->entityManager->getRepository(\HitmapsRoulette\DataAccess\Models\Mission::class)
            ->findOneBy(['slug' => $mission->slug]);

        /* @var $disguiseMapping DisguiseToIoiGuid */
        $disguiseMapping = $this->entityManager->getRepository(DisguiseToIoiGuid::class)
            ->findOneBy(['missionId' => $missionInfo->getId(), 'disguiseName' => $disguiseName]);
        if ($disguiseMapping === null) {
            throw new Exception("Could not find disguise for mission '{$mission->name}' and disguise '{$disguiseName}'!");
        }

        return $disguiseMapping->getIoiGuid();
    }

    private function generateGuid(){
        return strtolower(sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535)));
    }

    private function getAdditionalDisguiseGuidsForDisguise(string $disguiseGuid) {
        switch ($disguiseGuid) {
            case '4912d30a-80cb-41d8-8137-7b4727e76e4e':
                return ['e3256178-ce59-4796-bc5b-800cd6120b28'];
            case '78fc9e38-cade-42c3-958c-c7d8edf43713':
                return ['8b162546-0eab-40a0-a66b-a08e8ddf2ea4'];
            default:
                return [];
        }
    }
}
