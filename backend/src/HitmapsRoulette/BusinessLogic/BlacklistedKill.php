<?php


namespace HitmapsRoulette\BusinessLogic;


class BlacklistedKill {
    public $killName;
    public $disguise;

    public function __construct(string $killName, string $disguise) {
        $this->killName = $killName;
        $this->disguise = $disguise;
    }

    public function getKey() {
        return "{$this->killName}|{$this->disguise}";
    }
}
