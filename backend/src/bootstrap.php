<?php

use DI\ContainerBuilder;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\ORM\Tools\Setup;
use HitmapsRoulette\Config\Settings;
use Predis\Client;

require __DIR__ . '/../vendor/autoload.php';

$builder = new ContainerBuilder();
$settings = new Settings();

$applicationContext = $builder->build();

// region Doctrine
$databaseConfig = Setup::createConfiguration($settings->loggingEnvironment === 'production');
$driver = new AnnotationDriver(new AnnotationReader(), [__DIR__ . '/DataAccess/Models']);
AnnotationRegistry::registerLoader('class_exists');
$databaseConfig->setMetadataDriverImpl($driver);
$databaseConnection = [
    'driver' => 'mysqli',
    'user' => $settings->databaseUser,
    'password' => $settings->databasePassword,
    'host' => $settings->databaseHost,
    'dbname' => $settings->databaseName,
    'charset' => 'utf8mb4'
];
$entityManager = EntityManager::create($databaseConnection, $databaseConfig);
$applicationContext->set(EntityManager::class, $entityManager);
// endregion

// region Redis
$redis = new Predis\Client([
    'scheme' => 'tcp',
    'host' => $settings->redisHost,
    'port' => $settings->redisPort
]);
$applicationContext->set(Client::class, $redis);
// endregion

ini_set('max_execution_time', 300);
error_reporting(0);